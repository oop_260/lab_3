import java.util.Scanner;

public class Problem4 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int num;
        int sum = 0;
        int count = 0;
        while(true){
            System.out.print("Please input number: ");
            num = sc.nextInt();
            if(num != 0){
                sum = sum + num;
                count++;
                System.out.println("Sum : "+sum+" Avg : "+(((double)sum)/count));
            }else{
                System.out.println("Bye");
                break;
            }
        }
    }
}
